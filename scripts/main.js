/* ============= TABLE OF CONTENT ============

  * ACTION PLUGINS
  * ScrollStatus
  * MENU
  * HEADER
  * TIMELINE
  * ABOUT COMPANY SLIDER
  * TABS
  * SCHEME
  * ACTION SCHEMES
  * DOCUMENTS
  * PARTNERS
  * CUSTOMERS
  * ACTION SLIDER
  * ACTIVITIES SLIDER
  * DOWNLOADS
  * SEQUENTION

 ==============================================*/


/**
 * ACTION PLUGINS
 */

// action fancybox
$('[data-fancybox="images"]').fancybox({});

$('[data-fancybox]').fancybox({
    youtube : {
        controls : 0,
        showinfo : 0
    },
    vimeo : {
        color : 'f00'
    }
});

// action inputmask
Inputmask({"mask": "+7 (999) 999-9999"}).mask('.input-phone');



/**
 * addZero helper
 */

function addZero(digitsLength, source){
    let text = source + '';
    while(text.length < digitsLength) {
      text = '0' + text;
    }
  return text;
}

/**
 * ScrollStatus
 */

class ScrollStatus {
  constructor(el) {
    this.target = el;
  }

  get progress() {
    const { bottom, height } = this.target.getBoundingClientRect();
    let progress = 1 - (bottom / (window.innerHeight * (height / window.innerHeight + 1)));

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get isView(){
    return this.progress > 0 && this.progress < 1
  }

  get enterProgress() {
    const { top, height } = this.target.getBoundingClientRect();
    let progress = 1 - top / window.innerHeight;

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get isEnter() {
     return this.enterProgress > 0 && this.enterProgress < 1
  }

  get leaveProgress() {
    const { bottom } = this.target.getBoundingClientRect();
    let progress = 1 - bottom / window.innerHeight;

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get isLeave() {
     return this.leaveProgress > 0 && this.leaveProgress < 1
  }

  get screenViewProgress() {
    const rect = this.target.getBoundingClientRect();
    const { bottom, height } = rect;
    let progress = 1 - (bottom - window.innerHeight) / (height - window.innerHeight);

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }
}

/**
 * MENU
 */

class Menu {
  constructor(el) {
    this._el = el;
    this._list = this._el.querySelector('.menu__list');
    this._items = [...this._list.children];
    this._items.forEach(item => item.addEventListener('click', this.itemClickHandler.bind(this, event, item)));
  }

  itemClickHandler(e, item) {
    this._items.forEach(item => {
      item.classList.remove('menu__item_open');
    });
    item.classList.add('menu__item_open');
  }

  static toggle() {
    document.documentElement.classList.toggle('menu--open')
  }
}

// Action menu
document.querySelectorAll('.menu').forEach(elMenu => {
  const menu = new Menu(elMenu);

  document.querySelectorAll('.menu-burger').forEach(elBurger => {
    elBurger.addEventListener('click', Menu.toggle);
  })

});




/**
 * HEADER
 */

$(".header-promo-solution .owl-carousel").owlCarousel({
  items: 1,
  loop: true,
  autoHeight: true,
  nav: true,
  navText: ['', ''],
  dots: false,
});




/**
 * TIMELINE
 */

function timeline(elTimeline, $slider) {
  const items = getTimelineItems($slider);
  const timelineItems = createItems();
  const carusel = $slider.data()['owl.carousel'];
  const caruselItems = carusel.items();

  items[0].el.classList.add('timeline__item_active');

  $slider.on('changed.owl.carousel', function(event) {
    let activeIndex = 0;
    items.forEach((item, i) => {
      item.el.classList.remove('timeline__item_active');
      if (item.index <= event.item.index) {
        activeIndex = i;
      }
    });

    const firstIndex = items[activeIndex].index;
    const lastIndex = !!items[activeIndex + 1] ? items[activeIndex + 1].index : Infinity;

    items[activeIndex].el.classList.add('timeline__item_active');

    caruselItems.forEach((item, i) => {
      const slide = item[0].querySelector('.about-company-slider__slide');
      slide.classList.remove('about-company-slider__slide_active');
      if (firstIndex <= i && lastIndex > i) {
        slide.classList.add('about-company-slider__slide_active');
      }
    })
  });


  function getTimelineItems($slider) {
    const timelineItems = [];
    const yearsArr = [];
    $slider.find('.about-company-slider__slide').each(function(index) {
      const $year = $(this).find('.about-company-timeline-date__year')[0];
      const year = parseInt($year.innerHTML);
      if(!yearsArr.includes(year)) {
        yearsArr.push(year);
        timelineItems.push({
          index: index,
          year: year,
        });
      }
    });

    return timelineItems;
  }

  function createItem(item) {
    const elItem = document.createElement('div');
    item.el = elItem;
    elItem.classList.add('timeline__item');
    elItem.innerHTML = `
    <div class="timeline__item-trigger">
      <div class="timeline__item-circle"></div>
      <div class="timeline__item-label">${item.year}</div>
    </div>
    `;
    elItem.querySelector('.timeline__item-trigger').addEventListener('click', e => {
      $slider.trigger('to.owl.carousel', [item.index, 500, true]);
      items.forEach(item => item.el.classList.remove('timeline__item_active'));
      item.el.classList.add('timeline__item_active')
    });

    return elItem;
  }

  function createItems() {
    const itemsList = [];
    elTimeline.innerHTML = '';
    for (let item of items) {
      let elItem = createItem(item, $slider);
      elTimeline.appendChild(elItem);
      itemsList.push(elItem);
    }

    return itemsList;
  }
}



/**
 * ABOUT COMPANY SLIDER
 */

document.querySelectorAll('.about-company-slider').forEach(elAboutCompanySlider => {
  const $slider = $(elAboutCompanySlider).find(".owl-carousel");

  $slider.owlCarousel({
    loop: false,
    nav: true,
    navText: ['<span>назад</span>', '<span>далее</span>'],
    navContainer: '.about-company .owl-nav',
    pagination : true,
    margin: 20,
    responsive : {
      0: { items: 1 },
      780: { items: 3 },
    },
    onInitialized: function() {
      // Костыль для запуска триггеров обновления
      setTimeout(() => {
        this.next();
        setTimeout(() => {
          this.prev();
        })
      })
    }
  });

  const elTimeline = elAboutCompanySlider.querySelector('.timeline');

  if(!!elTimeline) timeline(elTimeline, $slider);
});



/**
 * TABS
 */

class Tabs {
  constructor(el) {
    this.target   = el;
    this.navItems = [...el.querySelectorAll('.tabs__nav-item')];
    this.navLinks = [...el.querySelectorAll('.tabs__nav-link')];
    this.items    = [...el.querySelectorAll('.tabs__item')];

    this.navLinks.forEach(item => item.addEventListener('click', this.navClickHandler.bind(this)));

    window.addEventListener('load', () => {
      if (window.innerWidth > 760) {
        let maxHeightTab = 200;
        this.items.forEach((item => {
          item.style.display = 'block';
          requestAnimationFrame(() => {
            let height = item.offsetHeight;
            maxHeightTab = height > maxHeightTab ? height : maxHeightTab;
            requestAnimationFrame(() => item.style.display = '');
          })
        }));
        requestAnimationFrame(() => this.target.style.height = maxHeightTab + 'px');
      }
    });
  }

  navClickHandler(e) {
    e.preventDefault();

    const hash = e.target.hash;

    this.navItems.forEach(item => item.classList.remove('tabs__nav-item_active'));
    this.items.forEach(item => {
      if('#' + item.id === hash) {
        item.classList.add('tabs__item_active');
      } else {
        item.classList.remove('tabs__item_active');
      }
    });
    e.target.parentElement.classList.add('tabs__nav-item_active');

    return false;
  }
}



/**
 * SCHEME
 */

class Scheme {
  constructor(el) {
    this._el = el;
    this._lables = [...this._el.querySelectorAll('.scheme__label')];
    this._lables.forEach(label => {
      label.onmouseover = e => this.labelHandler(e, label.dataset.stage);
      label.onmouseout = e => this.labelHandler(e, '0');
    });
  }

  labelHandler(e, stage) {
    this._el.querySelectorAll('.scheme__image-stage').forEach(el => {
      if(el.dataset.stage === stage) {
        el.classList.add('scheme__image-stage_active');
      } else {
        el.classList.remove('scheme__image-stage_active');
      }
    });

    this._lables.forEach(el => {
      if(el.dataset.stage === stage) {
        el.classList.add('scheme__label_active');
      } else {
        el.classList.remove('scheme__label_active');
      }
    })
  }
}



/**
 *  ACTION SCHEMES
 */

document.querySelectorAll("#schemes").forEach(sectionSchemes => {
  const elementsSchemes = document.querySelectorAll('.scheme');
  const elTabs = document.querySelector('.schemes .tabs');

  if (!!elTabs) new Tabs(elTabs);
  elementsSchemes.forEach(el => new Scheme(el));
});

/* ACTION PARALLAX */
document.querySelectorAll('.parallax').forEach(elParallax => {
  const parallaxBg = elParallax.querySelector('.parallax__bg');
  const parallaxSvg = elParallax.querySelector('svg');
  const scrollStatusParallax = new ScrollStatus(elParallax);


  window.addEventListener('scroll', e => {
    const progress = scrollStatusParallax.progress;

    if (parallaxBg)  parallaxBg.style.transform = `translateY(${ 25 - (1 - progress) * 50 }%)`;
    if (parallaxSvg) parallaxSvg.style.transform = `translateY(${ 25 - (progress) * 50 }%)`;
  });
});


$('.documents-accordeon-header').click(function () {
  const hasnt = ! $(this).hasClass('documents-accordeon-header--active');

  $('.documents-accordeon-header').removeClass('documents-accordeon-header--active');

  if (hasnt) {
    $(this).addClass('documents-accordeon-header--active');
  }
});




/**
 * DOCUMENTS
 */

$('[data-js-documents-slider]').each((function () {
  const index = $(this).data('slider-index');

  $(this).find('.owl-carousel').owlCarousel({
    loop: false,
    nav: true,
    navText: ['', ''],
    navContainer: `[data-slider-index="${index}"] .owl-nav`,
    dots: false,
    margin: 20,
    responsive : {
      0: { items: 1, },
      780: { items: 2 },
      1240: { items: 3 },
      1600: { items: 4 },
    }
  });
}));




/**
 * PARTNERS
 */

document.querySelectorAll('.partners').forEach(elPartners => {
  const $partners = $(elPartners);
  const $carousel = $(".partners-slider .owl-carousel");
  const $pagination = $partners.find('.carousel-pagination');
  const $paginationIndex = $pagination.find('.carousel-pagination__index');
  const $paginationProgress = $pagination.find('.carousel-pagination__progress');
  const $paginationCount = $pagination.find('.carousel-pagination__count');

  const cbUpdatePagination = function(e) {
    let count = e.page.count;
    let index = e.page.index;
    let progress = index / (count-1);

    $paginationIndex.html(index + 1);
    $paginationCount.html(count);
    $paginationProgress.css({
      transform: `translateX(-${(1 - progress) * 100}%)`
    });

    if (progress >= 1) {
      $paginationCount.css({
        color: '#C10230'
      })
    } else {
      $paginationCount.css({
        color: ''
      })
    }
  };

  $carousel.owlCarousel({
    loop: true,
    navText: ['', ''],
    dots: true,
    navContainer: '.partners .owl-nav',
    autoWidth: true,
    margin: 20,
    onChanged: cbUpdatePagination,
    onInitialized: function() {
      // Костыль от обновления нумерации страниц после инициализации
      // событие onInitialized выдает не те данные
      setTimeout(() => {
        this.next();
        setTimeout(() => {
          this.prev();
        })
      })
    }
  });
});




/**
 * CUSTOMERS
 */

document.querySelectorAll('.customers').forEach(elCustomers => {
  const $customers = $(elCustomers);
  const $carousel = $(".customers-slider .owl-carousel");
  const $pagination = $customers.find('.carousel-pagination');
  const $paginationIndex = $pagination.find('.carousel-pagination__index');
  const $paginationProgress = $pagination.find('.carousel-pagination__progress');
  const $paginationCount = $pagination.find('.carousel-pagination__count');


  const cbUpdatePagination = function(e) {
    let count = e.page.count;
    let index = e.page.index;
    let progress = index / (count-1);

    $paginationIndex.html(index + 1);
    $paginationCount.html(count);
    $paginationProgress.css({
      transform: `translateX(-${(1 - progress) * 100}%)`
    });

    if (progress >= 1) {
      $paginationCount.css({
        color: '#C10230'
      })
    } else {
      $paginationCount.css({
        color: ''
      })
    }
  };

  $carousel.owlCarousel({
    loop: true,
    navText: ['', ''],
    dots: true,
    navContainer: '.customers .owl-nav',
    autoWidth: true,
    margin: 20,
    onChanged: cbUpdatePagination,
    onInitialized: function() {
      // Костыль от обновления нумерации страниц после инициализации
      // событие onInitialized выдает не те данные
      setTimeout(() => {
        this.next();
        setTimeout(() => {
          this.prev();
        })
      })
    }
  });
});



/**
 * ACTION SLIDER
 */

document.querySelectorAll('.slider').forEach(elSlider => {
  const $slider = $(elSlider);
  const $carousel = $slider.find(".owl-carousel");
  const $pagination = $slider.find('.carousel-pagination');
  const $paginationIndex = $pagination.find('.carousel-pagination__index');
  const $paginationProgress = $pagination.find('.carousel-pagination__progress');
  const $paginationCount = $pagination.find('.carousel-pagination__count');


  const cbUpdatePagination = function(e) {
    let count = e.page.count;
    let index = e.page.index;
    let progress = index / (count-1);

    $paginationIndex.html(index + 1);
    $paginationCount.html(count);
    $paginationProgress.css({
      transform: `translateX(-${(1 - progress) * 100}%)`
    });

    if (progress >= 1) {
      $paginationCount.css({
        color: '#C10230'
      })
    } else {
      $paginationCount.css({
        color: ''
      })
    }
  };

  $carousel.owlCarousel({
    loop: true,
    navText: ['', ''],
    dots: true,
    navContainer: '.slider .owl-nav',
    items: 1,
    margin: 20,
    onChanged: cbUpdatePagination,
    onInitialized: function() {
      // Костыль от обновления нумерации страниц после инициализации
      // событие onInitialized выдает не те данные
      setTimeout(() => {
        this.next();
        setTimeout(() => {
          this.prev();
        })
      })
    }
  });
});



/**
 * ACTIVITIES SLIDER
 */

document.querySelectorAll('.activities-slider').forEach(elSlider => {
  const elementsSlides = elSlider.querySelectorAll('.activities-slider__slide');
  const elPrev         = elSlider.querySelector('.activities-slider__prev');
  const elNext         = elSlider.querySelector('.activities-slider__next');
  const elPagination   = elSlider.querySelector('.carousel-pagination');
  const elIndex        = elSlider.querySelector('.carousel-pagination__index');
  const elProgress     = elSlider.querySelector('.carousel-pagination__progress');
  const elCount        = elSlider.querySelector('.carousel-pagination__count');

  let intervar;
  let duration = 5000;
  let activeSlide = [...elementsSlides].findIndex(el => el.classList.contains('activities-slider__slide_active'));

  if (activeSlide < 0) {
    activeSlide = 0;
  }

  const updatePagination = () => {
    let progress = activeSlide / (elementsSlides.length - 1);

    elIndex.innerHTML = activeSlide + 1;
    elCount.innerHTML = elementsSlides.length;


    elProgress.style.transform = `translateX(-${(1 - progress) * 100}%)`;

    if (progress >= 1) {
      elPagination.classList.add('carousel-pagination_full');
    } else {
      elPagination.classList.remove('carousel-pagination_full');
    }
  }

  updatePagination();

  const switchSlide = index => {
    let prevSlide = activeSlide;

    activeSlide = index;
    if (activeSlide >= elementsSlides.length) activeSlide = 0;
    if (activeSlide < 0) activeSlide = elementsSlides.length - 1;


    elementsSlides.forEach(el => el.classList.remove('activities-slider__slide_active'));
    elementsSlides[activeSlide].classList.add('activities-slider__slide_active');


    updatePagination();

    clearInterval(intervar);
    intervar = setInterval(() => switchSlide(activeSlide + 1), duration);
  };

  intervar = setInterval(() => switchSlide(activeSlide + 1), duration);

  elNext.onclick = () => switchSlide(activeSlide + 1);
  elPrev.onclick = () => switchSlide(activeSlide - 1);
});


/**
 * DOWNLOADS
 */

document.querySelectorAll('.downloads').forEach(elDownloads => {

  const elmsTabs = elDownloads.querySelectorAll('.downloads__tab');

  elmsTabs.forEach(elTab => {
    const elHeader = elTab.querySelector('.downloads__tab-header');
    elHeader.onclick = e => {
       elmsTabs.forEach(el => el.classList.remove('downloads__tab_active'));
       elTab.classList.add('downloads__tab_active');
    };
  });

});

/**
 * APPLYING
 */

document.querySelectorAll('.applying__item').forEach(elItem => {
  const elImg = elItem.querySelector('.applying__img img');
  const src = elImg.src;
  const srcHover = elImg.dataset.srcHover;

  let elImgHover = new Image();
  let imgAvailable = false;
  elImgHover.src = srcHover;
  elImgHover.onload  = () => imgAvailable = true;

  let timerHover;

  elItem.onmouseover = e => {
    clearTimeout(timerHover);
    timerHover = setTimeout(() => {
      if (imgAvailable && elImg.src !== elImgHover.src) elImg.src = elImgHover.src
    }, 300);
  };

  elItem.onmouseout = e => {
    clearTimeout(timerHover);
    timerHover = setTimeout(() => {
      if (elImg.src !== src) elImg.src = src
    }, 300);
  };

});

/**
 * Sequention
 */

class Sequention {
  constructor(elContainer, { path, prefix, extension, digitsLength, count }) {

    this._active = 0;
    this._progress = 0;

    this.el           = elContainer;
    this.path         = path;
    this.prefix       = prefix;
    this.extension    = extension;
    this.count        = parseInt(count);
    this.digitsLength = parseInt(digitsLength);

    this.frames = this.createFrames();

  }

  createFrames() {
    let frames = [];
    this.el.innerHTML = '';

    for (let i = 0; i <= this.count; i++) {
      let frame = {};

      let img = new Image();
      let src = this.path + this.prefix + addZero(this.digitsLength, i) + '.' + this.extension;

      img.classList.add('sequention__frame');
      img.src = src;

      this.el.appendChild(img);

      frames[i] = img;
    }

    frames[this._active].classList.add('sequention__frame_active');

    return frames;
  }

  set active(val) {
    const prev = this._active;
    if (val === prev) return false;

    this._active = val;
    if (val > this.count) this.active = this.count;
    if (val < 0) this.active = 0;

    requestAnimationFrame(() => {
      this.frames[prev].classList.remove('sequention__frame_active');
      this.frames[this._active].classList.add('sequention__frame_active');
    });
  }

  get active() {
    return this._active;
  }

  set progress(val) {
    this._progress = val;
    if (this._progress > 1) this._progress = 1;
    if (this._progress < 0) this._progress = 0;
    this.active = parseInt(val * this.count);
  }

  get progress() {
    return this._progress;
  }
}



document.querySelectorAll('.scatter').forEach(elScatter => {
  const elSequention = elScatter.querySelector('.sequention');
  const params = elSequention.dataset;

  const sequention = new Sequention(elSequention, params);

  const scrollStatus = new ScrollStatus(elScatter);

  window.addEventListener('scroll', e => {
    const { screenViewProgress } = scrollStatus;

    sequention.progress = screenViewProgress;
  });

});



